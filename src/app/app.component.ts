import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
   templateUrl: './app.component.html',
  // template:'<h2 [textContent]="title" ]></h2>',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // courses = [1, 2, 3];
  // title='this is a title';
}
