import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyheadphonesComponent } from './buyheadphones.component';

describe('BuyheadphonesComponent', () => {
  let component: BuyheadphonesComponent;
  let fixture: ComponentFixture<BuyheadphonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyheadphonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyheadphonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
