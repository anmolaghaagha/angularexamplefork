import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhyheadphonesComponent } from './whyheadphones.component';

describe('WhyheadphonesComponent', () => {
  let component: WhyheadphonesComponent;
  let fixture: ComponentFixture<WhyheadphonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhyheadphonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhyheadphonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
