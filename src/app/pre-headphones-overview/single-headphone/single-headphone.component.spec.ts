import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleHeadphoneComponent } from './single-headphone.component';

describe('SingleHeadphoneComponent', () => {
  let component: SingleHeadphoneComponent;
  let fixture: ComponentFixture<SingleHeadphoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleHeadphoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleHeadphoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
